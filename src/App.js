import React, { Component } from 'react';
import Lottery, {rand} from './Lottery'
import './App.css';

class App extends Component {
    state = {
        numbers: []
    };

    generateUniqueNumbers = (sortedNumbers) => {
        let removedDuplicate = this.removeDuplicate(sortedNumbers);
        if (!removedDuplicate) {
            console.log(sortedNumbers, 'final sorted Numbers')
            return sortedNumbers;
        }
        console.log(removedDuplicate, 'removedDuplicate')
        let newNumbers = removedDuplicate.map(number => {
            console.log(number, 'number')
            if (number === 'undefined') {
                const newNumber = rand(5, 36);
                console.log(newNumber, 'newNumber')
                return newNumber;
            }
            return number;
        });
        console.log(newNumbers, 'newNumbers')
        let sortedNewNumbers = newNumbers.sort((a, b) => a > b)
        console.log(sortedNewNumbers, 'sortedNewNumbers')
        return this.generateUniqueNumbers(sortedNewNumbers);

    };

    removeDuplicate = sortedNumbers => {
        let result = false;
        for (let i=0; i<sortedNumbers.length; i++) {
            if (i === sortedNumbers.length - 1) {
                break;
            }
            if (sortedNumbers[i] === sortedNumbers[i+1]) {
                sortedNumbers[i] = 'undefined';
                result = sortedNumbers;
                break;
            }
        }
        return result;
    };


    generateSortedNumbers = () => [rand(5, 36), rand(5, 36), rand(5, 36), rand(5, 36), rand(5, 36)].sort((a, b) => a > b)

    componentDidMount = () => {
        this.newNumbers();
    };

    newNumbers = () => {
        const sortedNumbers = this.generateSortedNumbers();
        const uniqueNumbers = this.generateUniqueNumbers(sortedNumbers);
        console.log(uniqueNumbers, 'unique numbers in new numbers');
        this.setState({numbers: uniqueNumbers});
    };

    handleClick = () => {
        this.newNumbers();
    };


    render = () => {
        return (
            <div>
                <button className="button" onClick={this.handleClick}>New numbers</button>
                <div className="numbers">
                    {this.state.numbers.map((number, index) => <Lottery number={number} key={index} />)}
                </div>
            </div>
        );
    }
}

export default App;
