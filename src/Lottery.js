import React from 'react';

const Lottery = (props) => {
    return (
      <div className="lottery_number">{props.number}</div>
    );
};

export const rand = (min, max) => {
    return Math.floor(Math.random() * (max - min)) + min;
};
export default Lottery;